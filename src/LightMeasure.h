#ifndef LIGHTMEASURE_H
#define LIGHTMEASURE_H

#include <Arduino.h>

enum LightMeasureStates {
    LIGHTMEASURE_BEGIN,
    LIGHTMEASURE_MEASURE
};

class LightMeasure {
    private:
        LightMeasureStates _state;
        String _measure = "";
        int _analogPin;

        // Button single push
        int _btnPin = 8;
        unsigned long _delayOut = 1000; //ms
        unsigned long _pushTime = 0;

        // Measures timings
        unsigned long _delay = 1000; // ms
        unsigned long _start;
        unsigned long _lastMeasure;

        
    public:
        LightMeasure();
        void begin(int analogPin);
        void begin(int analogPin, unsigned long delay);
        void iterate();
        void add(unsigned long time, int value);
        String toCSV();
};

#endif