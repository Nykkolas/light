#include <Arduino.h>
#include "LightMeasure.h"

LightMeasure::LightMeasure() {
    // Do nothing
}

String LightMeasure::toCSV() {
    return _measure;
}

void LightMeasure::add(unsigned long time, int value) {
    _measure += String(time) + "," + String(value) + "\n";
    
    return;
}

void LightMeasure::begin(int analogPin) {
    _measure = "";
    _analogPin = analogPin;
    _start = millis();
    _state = LIGHTMEASURE_BEGIN;

    // button
    pinMode(_btnPin, INPUT);

    return;
}

void LightMeasure::begin(int analogPin, unsigned long delay) {
    begin(analogPin);

    _delay = delay;
}

void LightMeasure::iterate() {
    switch(_state) {
        case LIGHTMEASURE_BEGIN:
            add(millis(), analogRead(_analogPin));
            _lastMeasure = millis();
            _state = LIGHTMEASURE_MEASURE;
            break;
        case LIGHTMEASURE_MEASURE:
            if (millis() - _lastMeasure >= _delay) {
                _lastMeasure = millis();
                add(_lastMeasure, analogRead(_analogPin));
            }
            
            if (Serial.available()) {
                char c = Serial.read() - '0';
                
                if (c == 0) {
                    Serial.print(toCSV());
                }
            }

            if (digitalRead(_btnPin) == HIGH && !_pushTime) {
                _pushTime = millis();
                Serial.print(toCSV());
            }

            if (millis() - _pushTime > _delayOut) {
                _pushTime = 0;
            }

            _state = LIGHTMEASURE_MEASURE;
            break;
    }
    return;
}
