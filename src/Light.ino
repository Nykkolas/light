#include <Arduino.h>
#include "LightMeasure.h"

LightMeasure measure;
const int analogPin = A0;

void setup() {
    measure.begin(A0, 60000 * 15);
    Serial.begin(115200);
}

void loop() {
    measure.iterate();
}
