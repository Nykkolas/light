#include <ArduinoUnitTests.h>
#include "Arduino.h"
#include "LightMeasure.h"

LightMeasure testMeasure;
const int analogPin = 0;

unittest_setup()
{
  testMeasure = LightMeasure();
}

unittest(test_empty_measure) {
  string expected = "";
  string result;

  result = testMeasure.toCSV();

  assertEqual(expected, result);
}

unittest(test_one_measure) {
  string expected = "0,34\n";
  string result;

  testMeasure.add(0, 34);
  
  result = testMeasure.toCSV();

  assertEqual(expected, result);
}

unittest(test_two_measures) {
  string expected = "0,34\n98723847294,45\n";
  string result;

  testMeasure.add(0, 34);
  testMeasure.add(98723847294,45);
  
  result = testMeasure.toCSV();

  assertEqual(expected, result);
}

unittest(test_begin) {
  string expected = "";
  string result;
  
  testMeasure.add(0,34);

  testMeasure.begin(0);
  result = testMeasure.toCSV();

  assertEqual(expected, result);
}

unittest(test_one_iteration) {
  GodmodeState* godState = GODMODE();
  string expected = "0,34\n";
  string result;
  
  godState->reset();
  godState->analogPin[analogPin] = 34;

  testMeasure.begin(analogPin);

  testMeasure.iterate();

  result = testMeasure.toCSV();

  assertEqual(expected, result);
}

unittest (test_add_timing) {
  GodmodeState* state = GODMODE();
  string expected1 = "0,34\n";
  string expected2 = "0,34\n1001,567\n";
  int delayMes = 1000; // ms

  state->reset();
  state->analogPin[analogPin] = 34;

  testMeasure.begin(analogPin, delayMes);
  testMeasure.iterate();

  state->micros = delayMes * 1000 - 1000;
  testMeasure.iterate();
  assertEqual(expected1, testMeasure.toCSV());

  state->analogPin[analogPin] = 567;
  state->micros = delayMes * 1000 + 1000;
  testMeasure.iterate();
  assertEqual(expected2, testMeasure.toCSV());
}

unittest(test_display) {
  GodmodeState* state = GODMODE();
  string expected = "0,34\n1001,567\n";
  state->serialPort[0].dataOut = "";
  int delayMes = 1000; // ms

  state->reset();
  state->analogPin[analogPin] = 34;
  testMeasure.begin(analogPin, delayMes);
  testMeasure.iterate();
  state->analogPin[analogPin] = 567;
  state->micros = delayMes * 1000 + 1000;
  testMeasure.iterate();

  state->serialPort[0].dataIn = "0";
  testMeasure.iterate();

  assertEqual(expected, state->serialPort[0].dataOut);
}

unittest(test_round_timing) {
  GodmodeState* state = GODMODE();
  string expected = "0,34\n1000,567\n";
  unsigned long delayMes = 1000; // ms

  state->reset();
  state->analogPin[analogPin] = 34;

  testMeasure.begin(analogPin, delayMes);
  testMeasure.iterate();

  state->analogPin[analogPin] = 567;
  state->micros = delayMes * 1000;
  testMeasure.iterate();
  assertEqual(expected, testMeasure.toCSV());
}

unittest(test_button) {
  GodmodeState* state = GODMODE();
  string expected = "0,34\n";
  unsigned long delayMes = 1000; // ms

  state->reset();
  state->analogPin[analogPin] = 34;

  testMeasure.begin(analogPin, delayMes);
  testMeasure.iterate();

  state->digitalPin[8] = HIGH;
  testMeasure.iterate();
  
  assertEqual(expected, state->serialPort[0].dataOut);
}

unittest_main()
